setlocal shiftwidth=2
setlocal softtabstop=2

setlocal textwidth=120
setlocal colorcolumn=+1

let b:delimitMate_expand_cr = 1
let b:delimitMate_expand_space = 1
