setlocal shiftwidth=2
setlocal softtabstop=2

let b:delimitMate_expand_space = 1
let b:delimitMate_expand_cr = 1
