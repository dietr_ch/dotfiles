setlocal shiftwidth=2
setlocal softtabstop=2

setlocal foldmethod=indent
setlocal foldignore-=#
setlocal foldnestmax=3
setlocal foldminlines=5
