" Colours
set background=dark
try
    colorscheme base16-bright
catch /^Vim\%((\a\+)\)\=:E185/
endtry
